export default (_) => {
  function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  return {
    validateForRequiredFieldErrors: (fieldName, value) => _.cond([
      [
        _.isNil,
        _.constant(fieldName + ' is required'),
      ],
    ])(value),
    validateEmail,

    validateForEmailErrors: _.cond([
      [
        _.isNil,
        _.constant('Email is required'),
      ],
      [
        _.negate(validateEmail),
        _.constant('Invalid email'),
      ],
    ]),

    validateForUsernameErrors: _.cond([
      [
        _.isNil,
        _.constant('Username is required'),
      ],
      [
        str => str.length < 5,
        _.constant('Username must be 5 or more characters'),
      ],
      [
        str => str.match(/\s+/g),
        _.constant('Username can only include alphanumeric characters'),
      ],
    ]),

    validateForPasswordErrors: _.cond([
      [
        _.negate(_.isString),
        _.constant('Invalid password'),
      ],
      [
        _.isNil,
        _.constant('Password is required'),
      ],
      [
        str => str.length < 5,
        _.constant('Password must be 5 or more characters'),
      ],
      [
        str => str.match(/\s+/g),
        _.constant('Password can only include alphanumeric characters'),
      ],
    ]),
  };
}
