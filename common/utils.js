export function parseCookieFromString(cookies, cookieName) {
  const cookiestring = RegExp('' + cookieName + '[^;]+').exec(cookies);
  // Return everything after the equal sign, or an empty string if the cookie name not found
  return decodeURIComponent(!!cookiestring ?
      cookiestring.toString().replace(/^[^=]+./, '') :
      '');
}