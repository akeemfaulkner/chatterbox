import {MissingArgumentError} from '../../main/common/errors';

describe('common/errors', () => {

  describe('MissingArgumentError', () => {

    const argumentNames = ['apple', 'pears'];
    const callerName = 'myFunction';

    test('It is an instance of error', () => {
      expect(new MissingArgumentError(argumentNames, callerName) instanceof Error).toBeTruthy()
    });

    test(
        'The message should include the name of the missing arguments and the caller',
        () => {

          const error = new MissingArgumentError(argumentNames, callerName);

          expect(error.message).toEqual(
              expect.stringContaining(JSON.stringify(argumentNames)),
          );

          expect(error.message).toEqual(
              expect.stringContaining(callerName),
          );

        });
  });

});