import {UserDto} from '../../main/module/user/response';

describe('user/response', () => {

  test('UserDto removes the password', () => {

    const mockUser = {
      id: 1,
      username: 'username',
      email: 'email',
      password: 'password',
      profile_photo: 'profile_photo',
      bio: 'bio'
    };

    const userDto = UserDto(mockUser) as any;

    expect(userDto.password).
        toBeUndefined();

  });

  test('UserDto returns null if there is no user provided', () => {
    expect(UserDto(undefined as any)).
        toBeNull();
  });
});