import app from '../../main/app';
import supertest from 'supertest';
import {authenticateUser} from '../__helpers__';
import {users} from '../../../seeds/test/users';
import {UserDto} from '../../main/module/user/response';
import {ErrorDto, StatusDto} from '../../main/common/response';
import {UserCreationError} from '../../main/module/user/errors';

type UserDtoType = ReturnType<typeof UserDto>;
describe('User API Integration Tests', () => {

  test('It returns 401 if not authenticated', () => {

    const get = supertest(app).
        get('/users').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    const getProfile = supertest(app).
        get('/profile').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    const putProfile = supertest(app).
        put('/profile').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    const deleteProfile = supertest(app).
        delete('/profile').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    const putProfileUsername = supertest(app).
        put('/profile/username').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    return Promise.all([
      get,
      getProfile,
      putProfile,
      deleteProfile,
      putProfileUsername,
    ]);
  });

  test('It returns 200 with response if authenticated', async () => {

    const {token} = await authenticateUser();

    return supertest(app).
        get('/users').
        set('Authorization', `Bearer ${token}`).
        then(function(res) {
          const data: Array<UserDtoType> = res.body.data;

          expect(res.statusCode).toBe(200);
          expect(data.find(user => user.username === users[0].username)).
              toBeTruthy();
          expect(data.find(user => user.username === users[1].username)).
              toBeTruthy();
        });
  });

  describe('Create User', () => {
    const newUser = {
      'username': 'newUser',
      'first_name': 'first_name',
      'last_name': 'last_name',
      'email': 'my@email.com',
      'password': 'strongpassword',
    };

    test('It creates a user if the user does not exist', async () => {

      return supertest(app).
          post('/users').
          send(newUser).
          then(function(res) {
            expect(res.statusCode).toBe(201);
            expect(res.body).toEqual(StatusDto(true));
          });
    });

    test('It does not create a user if the user exists', async () => {
      const post = () => supertest(app).
          post('/users').
          send(newUser);

      return Promise.all([
        post(),
        post(),
      ]).
          then(function([first, second]) {
            expect(second.statusCode).toBe(400);
            expect(second.body).
                toEqual(StatusDto(false, ErrorDto(new UserCreationError())));
          });
    });

    test('It fails on invalid password', () => {
      const invalidPasswordUser = {
        'username': 'invalidPasswordUser',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'email': 'my@email.com',
        'password': '',
      };
      return supertest(app).
          post('/users').
          send(invalidPasswordUser).
          then(function(res) {
            expect(res.statusCode).toBe(400);
          });
    });

    test('It fails on invalid username', () => {
      const invalidPasswordUser = {
        'username': '',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'email': 'my@email.com',
        'password': 'password',
      };
      return supertest(app).
          post('/users').
          send(invalidPasswordUser).
          then(function(res) {
            expect(res.statusCode).toBe(400);
          });
    });

  });
});