import {ErrorDto, StatusDto} from '../../main/common/response';

describe('common/response', () => {

  test('StatusDto returns the expected formatted data', () => {

    const expected_1 = {foo: 'bar'};
    const status_1 = true;

    const expected_2 = {bar: 'foo'};
    const status_2 = false;

    expect(StatusDto(status_1, expected_1)).toMatchSnapshot();

    expect(StatusDto(status_2, expected_2)).toMatchSnapshot();
  });

  test('ErrorDto returns the expected formatted data', () => {
    const error = new Error('this is an error');
    const data = {_meta: [1, 2, 3, 4, 5]};

    expect(ErrorDto(error, data)).toMatchSnapshot();
  });
});