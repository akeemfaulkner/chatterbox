import supertest from 'supertest';
import {users} from '../../../seeds/test/users';
import app from '../../main/app';
import {AuthenticationDto} from '../../main/module/authentication/response';

export function authenticateUser(userIndex: number = 0): Promise<ReturnType<typeof AuthenticationDto>> {
  const user = users[userIndex];

  return new Promise((resolve, reject) => {
    supertest(app).
        post('/authorise').
        send({username: user.username, password: user.password}).
        end(function(error, res) {
          if (error) {
            return reject(error);
          }
          resolve(res.body);
        });

  });
}

export async function createConversation() {
  const {token} = await authenticateUser(0);
  const recipient = users[1];
  return new Promise((resolve, reject) => {
    supertest(app).
        post('/conversation').
        send({recipientUsername: recipient.username}).
        set('Authorization', `Bearer ${token}`).
        end(function(error, res) {
          if (error) {
            return reject(error);
          }
          resolve(res.body);
        });

  });
}