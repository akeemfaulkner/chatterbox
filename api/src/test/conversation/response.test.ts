import {
  ConversationDto,
  ConversationListDto,
} from '../../main/module/conversation/response';

describe('conversation/response', () => {

  const message = {
    id: 123,
    user: 3,
    user_conversation: 45,
    message: '123',
    created_at: 123,
    updated_at: 123,
  };

  test('ConversationDto returns the expected formatted data', () => {

    const data = {
      id: 45,
      user_1: 4,
      user_2: 3,
      created_at: 12453453,
      updated_at: 12453453,
      messages: [
        message,
      ],
    };

    expect(ConversationDto(data, [])).toMatchSnapshot();

  });

  test('ConversationListDto returns the expected formatted data', () => {

    const data = [
      {
        id: 45,
        user_1: 4,
        user_2: 3,
        created_at: 12453453,
        updated_at: 12453453,
        last_message: message,
      }];

    const users = [
      {
        'id': 3,
        'username': 'test',
        'first_name': 'first',
        'last_name': 'last',
        'email': 'faulkner.development@gmail.com',
        'bio': null,
        'profile_photo': null,
        'password': 'password',
      },
      {
        'id': 4,
        'username': 'test1',
        'first_name': 'first',
        'last_name': 'last',
        'email': 'faulkner.development@gmail.com1',
        'bio': null,
        'profile_photo': null,
        'password': 'password',
      },
    ];

    expect(ConversationListDto(data, users)).toMatchSnapshot();

  });

});