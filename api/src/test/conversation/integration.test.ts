import app from '../../main/app';
import supertest from 'supertest';
import {authenticateUser, createConversation} from '../__helpers__';
import {users} from '../../../seeds/test/users';
import {UserDto} from '../../main/module/user/response';
import {ErrorDto, StatusDto} from '../../main/common/response';
import {UserCreationError} from '../../main/module/user/errors';

type UserDtoType = ReturnType<typeof UserDto>;
describe('Conversation API Integration Tests', () => {

  test('It returns 401 if not authenticated', () => {

    const get = supertest(app).
        get('/conversation/1').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    const put = supertest(app).
        put('/conversation/1').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    const post = supertest(app).
        post('/conversation').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    const del = supertest(app).
        delete('/conversation/1').
        then(function(res) {
          expect(res.statusCode).toBe(401);
        });

    return Promise.all([
      get,
      put,
      post,
      del,
    ]);
  });

  test('It returns a conversation', async () => {
    await createConversation();
    supertest(app).
        get('/conversation/1').
        then(function(res) {
          expect(res.statusCode).toBe(200);
        });
  });
});