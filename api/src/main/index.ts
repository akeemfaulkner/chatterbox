import server from './server';

export default server.listen(3000, () => console.log('Listening on http://localhost:3000'));

