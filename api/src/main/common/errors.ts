export class DomainError extends Error {
  __proto__: Error;
  constructor(message?: string) {
    const trueProto = new.target.prototype;
    super(message);

    // Alternatively use Object.setPrototypeOf if you have an ES6 environment.
    this.__proto__ = trueProto;
  }
}

export class MissingArgumentError extends DomainError {
  constructor(args?: any, callerName?: string) {
    const message = `Missing arguments ${JSON.stringify(args)} in caller ${callerName}`;
    super(message);
  }
}