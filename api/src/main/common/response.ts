export function StatusDto(succeeded: boolean, data: object = {}) {
  return {
    succeeded,
    data,
  };
}

export function ErrorDto(error: Error, data: object = {}) {
  return {
    error: error.message,
    data
  };
}
