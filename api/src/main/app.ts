import {authentication} from './module/authentication';
import {user} from './module/user';
import {conversation} from './module/conversation';
import {media} from './module/media';

require('./module/authentication');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
authentication(app);
user(app);
conversation(app);
media(app);

export default app;