module.exports = {
  FINGERPRINT_SALT: process.env.FINGERPRINT_SALT,
  JWT_SECRET: process.env.JWT_SECRET
};
