import KnexQueryBuilder from 'knex/lib/query/builder';

const env = process.env.NODE_ENV || 'development';
export const connection = require('knex')(require('../../../../knexfile')[env]);

export function PaginationResult(args = {
  total: 0,
  per_page: 0,
  offset: 0,
  to: 0,
  last_page: 0,
  current_page: 0,
  from: 0,
  data: [],
}) {

  return {
    total: args.total,
    per_page: args.per_page,
    offset: args.offset,
    to: args.to,
    last_page: args.last_page,
    current_page: args.current_page,
    from: args.from,
    data: args.data,
  };
}

KnexQueryBuilder.prototype.paginate = function(current_page, per_page) {
  const page = Math.max(current_page || 1, 1);
  const offset = (page - 1) * per_page;
  const clone = this.clone();

  return Promise.all([
    this.offset(offset).limit(per_page),
    connection.count('*').from(clone.as('t1')),
  ]).then(([rows, total]) => {
    const count = parseInt(total[0]['count(*)'] || 0, 10);

    return PaginationResult({
      total: count,
      per_page,
      offset,
      to: offset + rows.length,
      last_page: Math.ceil(count / per_page),
      current_page: page,
      from: offset,
      data: rows,
    });
  });
};

connection.queryBuilder = function() {
  return new KnexQueryBuilder(connection.client);
};
