export const selectFirst = <T = any>(rows: T[]): T | null =>
    rows.length === 0 ? null : rows[0];

export const noneThen =
    <P = any>(
        promise: () => Promise<P>, error?: { new(message?: any): Error }) =>
        async (rows: any[]) => {

          if (!rows || !selectFirst(rows)) {
            try {
              return await promise();
            } catch (e) {
              return Promise.reject(new error());
            }
          }

          return error
              ? Promise.reject(new error())
              : Promise.reject();
        };