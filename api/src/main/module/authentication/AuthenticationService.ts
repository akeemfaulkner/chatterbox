import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import UserRepository from '../user/UserRepository';
import {NoUserFoundError} from '../user/errors';
import keys from '../../config/keys';

const SALT_ROUNDS = 10;

export default new class AuthenticationService {

  authenticate(username: string, password: string, fingerprint: object) {
    return UserRepository.findUserByUsername(username).then(async (user) => {

      if (await this.isPasswordValid(user, password)) {
        return Promise.reject(new NoUserFoundError());
      }

      const encryptedFingerprint =
          await this.encrypt(
              this.stringifyFingerprint(fingerprint),
              1,
          );
      return this.getJWTToken(user.id, encryptedFingerprint);
    });
  }

  isPasswordValid = async (user, password) =>
      !user || !(await this.verifyEncryption(password, user.password));

  getJWTConfig() {
    return {
      secretOrKey: keys.JWT_SECRET,
    };
  }

  getJWTToken(id: number, fingerprint: string) {
    const options = this.getJWTConfig();
    const payload = {id, fingerprint};
    return jwt.sign(payload, options.secretOrKey);
  }

  decodeJWTToken(token: string): Promise<authentication.models.JWTToken> {
    return new Promise((resolve, reject) => {
      const options = this.getJWTConfig();
      jwt.verify(token, options.secretOrKey, (error, decoded) => {
        if (error) {
          return reject(error);
        }
        resolve(decoded);
      });
    });
  }

  async encrypt(value: string, rounds: number = SALT_ROUNDS): Promise<string> {
    return await bcrypt.hash(value, rounds);
  }

  async verifyEncryption(value: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(value, hash);
  }

  stringifyFingerprint(fingerprint: object) {
    return JSON.stringify(fingerprint) + keys.FINGERPRINT_SALT;
  }
};