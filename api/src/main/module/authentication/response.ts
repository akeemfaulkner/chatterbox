export function AuthenticationDto(token: string) {
  return {
    token,
    issued_at: Date.now(),
  };
}
