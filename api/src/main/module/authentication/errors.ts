import {DomainError} from '../../common/errors';

export class AuthenticationError extends DomainError {
  message = 'Incorrect username or password';
  name = 'AuthenticationError';
}
export class InvalidTokenError extends DomainError {
  message = 'Invalid token supplied';
  name = 'InvalidTokenError';
}
