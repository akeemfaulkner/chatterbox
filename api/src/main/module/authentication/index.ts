import {Application, Request} from 'express';
import AuthenticationController from './AuthenticationController';
import AuthenticationService from './AuthenticationService';
import UserRepository from '../user/UserRepository';
import keys from '../../config/keys';
import {InvalidTokenError} from './errors';

const _ = require('lodash/fp');
const Fingerprint = require('express-fingerprint');

const withFingerprint = (app: Application) => {

  app.use((req, res, next) => {
    return Fingerprint({
      parameters: [
        // Defaults
        Fingerprint.useragent,
        Fingerprint.acceptHeaders,
        Fingerprint.geoip,

        // Additional parameters
        function(next) {
          // ...do something...
          next(null, {
            'ip': req.connection.remoteAddress,
          });
        },
      ],
    })(req, res, next);
  });

  return app;
};

const withController = (app: Application) => {
  app.post(
      '/authorise',
      AuthenticationController.authorise,
  );

  return app;
};

function getAuthorisationHeader(req: Request) {

  const authorisation = (
      req.headers['authorization'] || req.headers['Authorization']
  ) as string;

  if (!authorisation) {
    throw new InvalidTokenError();
  }

  return authorisation.replace('Bearer ', '');
}

export const authenticatedRoute = async (req, res, next) => {

  try {

    const token = getAuthorisationHeader(req);

    const payload = await AuthenticationService.decodeJWTToken(token);

    const isValidFingerprint =
        await AuthenticationService.
            verifyEncryption(
                AuthenticationService.stringifyFingerprint(req.fingerprint),
                payload.fingerprint,
            );

    if (payload && isValidFingerprint) {
      req.user = await UserRepository.findUserById(payload.id);
    } else {
      return res.sendStatus(401);
    }

  } catch (e) {
    return res.sendStatus(401);
  }

  next();
};

export const authentication = _.compose(withController, withFingerprint);

