import {Request, Response} from 'express';
import AuthenticationService from './AuthenticationService';
import {AuthenticationDto} from './response';
import {AuthenticationError} from './errors';
import {ErrorDto, StatusDto} from '../../common/response';

export default new class AuthenticationController {
  authorise = (request: Request, response: Response) => {

    const {username, password} = request.body as authentication.models.AuthenticationPayload;

    return AuthenticationService.
        authenticate(username, password, request.fingerprint).
        then(token => {
          return response.json(AuthenticationDto(token));
        }).
        catch(() => {
          return response.
              status(400).
              json(StatusDto(false, ErrorDto(new AuthenticationError())));
        });

  };
};