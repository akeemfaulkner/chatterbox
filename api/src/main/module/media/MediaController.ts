import {Request, Response} from 'express';
import * as path from 'path';

const UPLOADS_PATH = path.resolve(__dirname, '../../../../data/', `uploads/`);

export default new class MediaController {

  get(request: Request, response: Response) {

    const options = {
      root: UPLOADS_PATH,
      dotfiles: 'deny',
      headers: {
        'x-timestamp': Date.now(),
        'x-sent': true,
        'Content-Type': 'image/jpeg',
      },
    };
    const fileName = request.params.path;
    response.sendFile(fileName, options, function(err) {
      if (err) {
        response.sendStatus(404)
      }
    });

  }

};
