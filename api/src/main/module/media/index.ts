import {Application} from 'express';
import MediaController from './MediaController';
import {authenticatedRoute} from '../authentication';


const withController = (app: Application) => {

  app.get(
      '/media/:path(*)',
      authenticatedRoute,
      MediaController.get,
  );
  return app;
};

export const media = withController;

