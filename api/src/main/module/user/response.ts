export function UserDto(user: user.models.User) {
  if (user) {
    const {
      id,
      username,
      email,
      bio,
      profile_photo,
    } = user;
    return {
      id,
      username,
      email,
      bio,
      profile_photo,
    };
  }
  return null;
}