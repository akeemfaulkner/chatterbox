import getValidators from '../../../../../common/validators';
import _ from 'lodash';

const validators = getValidators(_);

export default validators;