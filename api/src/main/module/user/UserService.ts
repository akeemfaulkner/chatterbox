import {noneThen} from '../database/utils';
import UserRepository from './UserRepository';
import {UserCreationError, UserExistsError} from './errors';
import validators from './validators';

export default new class UserService {

  createUser(user: user.models.NewUserPayload) {

    const {username, password, email} = user;

    const usernameError = validators.validateForUsernameErrors(username);
    const passwordError = validators.validateForPasswordErrors(password);
    const emailError = validators.validateForEmailErrors(email);

    if (usernameError) {
      return Promise.reject(new UserCreationError(usernameError));
    }

    if (passwordError) {
      return Promise.reject(new UserCreationError(passwordError));
    }

    if (emailError) {
      return Promise.reject(new UserCreationError(emailError));
    }

    return Promise.all([
      UserRepository.findUserByUsername(username),
      UserRepository.findUserByEmail(email),
    ]).then(([userByUsername, userByEmail]) => {

      if (userByUsername || userByEmail) {
        throw new UserExistsError();
      }

      return UserRepository.create(user);

    });
  }

  updateUser({bio, profile_photo, id}: Partial<user.models.User>) {
    return UserRepository.updateUser({id, bio, profile_photo});
  }

  updateUsername(user: user.models.NewUsernamePayload) {
    return UserRepository.updateUsername(user);
  }

  getNewUsername(username: string) {

    const getUniqueUsername = this.getUniqueUsername.bind(this, username);

    return UserRepository.findMatchingUsernames(username).
        then(getUniqueUsername);
  }

  getUniqueUsername(username: string, usernames: string []) {
    if (!usernames) {
      return undefined;
    }
    // Convert it to base 36 (numbers + letters), and grab the first 9 characters
    // after the decimal.
    const id = () => '_' + Math.random().toString(36).substr(2, 5);

    let attempt = username + id();

    while (usernames.find(u => u === attempt)) {
      attempt = username + id();
    }

    return attempt;
  }

};