import {connection} from '../database/index';
import {selectFirst} from '../database/utils/index';
import AuthenticationService from '../authentication/AuthenticationService';
import sanitizeHtml from 'sanitize-html';
import {MissingArgumentError} from '../../common/errors';
import {
  DuplicateUsernamesError,
  NoUserFoundError,
  UserCreationError,
} from './errors';

const clean = (input: string) => input ? sanitizeHtml(input, {
  allowedTags: [],
  allowedAttributes: {},
}) : null;

const noUserFound = () => new NoUserFoundError();

export default new class UserRepository {

  UserTable = () => connection('users');

  all = (
      like: string,
      excludedUsername: string,
      pageNumber: number = 1,
      perPage: number = 10): Knex.PaginationResponse => this.UserTable().
      andWhere(function(){
        const value = `%${like}%`;
        this.
            where('username', 'like', value).
            orWhere('email', 'like', value);
      }).
      andWhere('username', '!=', excludedUsername).
      select().
      paginate(pageNumber, perPage);

  findUserById(id: number) {

    return this.findWhere({id}).
        then(selectFirst).
        catch(noUserFound);
  }

  findUserByUsername(username: string) {
    return this.findWhere({username}).
        then(selectFirst).
        catch(noUserFound);
  }

  findUserByEmail(email: string) {
    return this.findWhere({email}).
        then(selectFirst).
        catch(noUserFound);
  }

  async create(user: user.models.NewUserPayload) {

    const {
      username,
      email,
      password,
    } = user;

    try {

      return await this.UserTable().
          insert({
            username: clean(username),
            email: clean(email),
            password: await AuthenticationService.encrypt(password),
            created_at: new Date(),
            updated_at: new Date(),
          });
    } catch (e) {
      throw new UserCreationError();
    }
  };

  updateUsername(user: user.models.NewUsernamePayload) {

    const {
      id,
      new_username,
      old_username,
    } = user;

    if (!new_username) {
      return Promise.reject(
          new MissingArgumentError('new_username', 'updateUsername'));
    }

    return this.findMatchingUsernames(new_username).
        then(usernames => {
          const matches = usernames.filter(
              username => username !== old_username);
          if (!matches.length) {
            return this.findWhere({id}).update({
              username: clean(new_username),
              updated_at: new Date(),
            });
          }

          return Promise.reject(new DuplicateUsernamesError(usernames));

        });
  }

  deleteUser({id}: user.models.DeleteUserPayload) {
    return this.findWhere({id}).del();
  }

  async updateUser(user: Partial<user.models.User>) {

    const {
      bio,
      id,
      profile_photo,
    } = user;

    return this.findWhere({id}).
        update({
          bio: clean(bio),
          profile_photo,
          updated_at: new Date(),
        });

  }

  findMatchingUsernames(username: string) {

    const mapToUsernames = rows =>
        rows.length && rows.map(row => row.username);

    return this.UserTable().
        andWhere('username', 'like', username).
        select('username').
        then(mapToUsernames);
  }

  findWhere(params: object) {
    return this.UserTable().where(params);
  }

};