import {Application} from 'express';
import multer from 'multer';
import UserController from './UserController';
import {authenticatedRoute} from '../authentication';
import * as path from 'path';
import * as fs from 'fs-extra';

const UPLOADS_PATH = path.resolve(__dirname, '../../../../data/', `uploads/`);
fs.mkdirsSync(UPLOADS_PATH);


const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    const user_uploads_path = path.resolve(UPLOADS_PATH, `${req.user.id}`);
    fs.mkdirs(user_uploads_path, function(err){
      cb(null, user_uploads_path);
    });

  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  fieldSize: 8 * 1024 * 1024,
});

const withController = (app: Application) => {

  app.get(
      '/users',
      authenticatedRoute,
      UserController.all,
  );

  app.post(
      '/users',
      UserController.create,
  );

  app.get(
      '/profile',
      authenticatedRoute,
      UserController.get,
  );

  app.put(
      '/profile',
      [
        authenticatedRoute,
        upload.fields([
          {name: 'profile_photo', maxCount: 1},
          {name: 'bio', maxCount: 1},
        ]),
      ],
      UserController.update,
  );

  app.put(
      '/profile/username',
      authenticatedRoute,
      UserController.updateUsername,
  );

  app.delete(
      '/profile',
      authenticatedRoute,
      UserController.deleteUser,
  );

  return app;
};

export const user = withController;

