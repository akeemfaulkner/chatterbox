import {DomainError} from '../../common/errors';

export class UserExistsError extends DomainError {
  message = 'This user already exists';
  name = 'UserExistsError';
}

export class DuplicateUsernamesError extends DomainError {
  usernames: string[];

  constructor(usernames: string[] = []) {
    super('There were duplicate usernames found');
    this.name = 'DuplicateUsernamesError';
    this.usernames = usernames;
  }
}

export class UserUpdateError extends DomainError {
  name = 'UserUpdateError';
  message = 'There was an error updating user details';
}

export class UserCreationError extends DomainError {
  name = 'UserCreationError';

  constructor(reason?: string) {
    super(
        reason
            ? reason
            : 'There was an error creating your account',
    );
  }
}

export class NoUserFoundError extends DomainError {
  name = 'NoUserFoundError';
  message = 'There was no user found';
}
