import {Request, Response} from 'express';
import UserService from './UserService';
import UserRepository from './UserRepository';
import * as Jimp from 'jimp';
import * as piexif from 'piexifjs';
import * as fs from 'fs-extra';
import {UserDto} from './response';
import {ErrorDto, StatusDto} from '../../common/response';
import {
  DuplicateUsernamesError,
  NoUserFoundError,
  UserExistsError,
  UserUpdateError,
} from './errors';
import {PaginationResult} from '../database';

export default new class UserController {

  async all(request: Request, response: Response) {
    const pageNumber = request.query.page_number || 1;
    const query = request.query.q;

    if(!query) {
      return response.
          status(200).
          json(PaginationResult())
    }

    try {

      const result =
          await UserRepository.all(query, request.user.username, pageNumber);

      return response.
          status(200).
          json({
            ...result,
            data: result.data.map(UserDto),
          });

    } catch (error) {

      return response.
          status(404).
          json(ErrorDto(error));

    }
  }

  get(request: Request, response: Response) {

    if (request.user) {
      return response.status(200).json(UserDto(request.user));
    }

    return response.status(404).json(ErrorDto(new NoUserFoundError()));

  }

  create = (request: Request, response: Response) => {

    const user: user.models.NewUserPayload = request.body;

    UserService.createUser(user).
        then(() =>
            response.
                status(201).
                json(StatusDto(true)),
        ).
        catch(this.handleCreateUserError(request, response));
  };

  async update(request: Request, response: Response) {

    const files = request.files && request.files['profile_photo'];

    if (files && files[0]) {

      const file = files[0];

      const respondError = () => response.
          status(400).
          json(StatusDto(false, new UserUpdateError()));

      try {

        const buffer = await Jimp.read(file.path).then(image => {
          return image.resize(Jimp.AUTO, 250).
              quality(100).
              getBufferAsync(Jimp.MIME_JPEG);
        });

        const encoding = 'binary';

        const image = piexif.remove(
            buffer.toString(encoding),
        );

        return fs.writeFile(file.path, Buffer.from(image, encoding), function(err) {

          if (err) {
            return respondError();
          }

          UserService.
              updateUser({
                bio: request.body.bio,
                profile_photo: `/media/${request.user.id}/${file.filename}`,
                id: request.user.id,
              }).
              then(() => response.json(StatusDto(true))).
              catch(() => response.
                  status(400).
                  json(StatusDto(false, new UserUpdateError())),
              );

        });
      } catch (e) {
        return respondError();
      }

    }
    UserService.
        updateUser({
          bio: request.body.bio,
          id: request.user.id,
        }).
        then(() => response.json(StatusDto(true))).
        catch(() => response.
            status(400).
            json(StatusDto(false, new UserUpdateError())),
        );
  }

  updateUsername = (request: Request, response: Response) => {

    return UserService.
        updateUsername({
          ...request.body,
          old_username: request.user.username,
          id: request.user.id,
        }).
        then(() => response.json(StatusDto(true))).
        catch(this.handleUpdateUsernameError(request, response));
  };

  deleteUser(request: Request, response: Response) {
    return UserRepository.
        deleteUser({id: request.user.id}).
        then(() => response.
            status(204).
            json(StatusDto(true)),
        ).
        catch(() => response.
            status(400).
            json(StatusDto(false)),
        );
  }

  private handleUpdateUsernameError =
      (request: Request, response: Response) =>
          async (error: Error) => {

            if (error instanceof DuplicateUsernamesError) {

              const user: user.models.NewUsernamePayload = request.body;
              const usernames = error.usernames;
              const alternativeUsernames: string[] = [];
              const getUniqueUsername =
                  UserService.getUniqueUsername.bind(this, user.new_username);

              for (let i = 0; i < 5; i++) {
                alternativeUsernames.push(
                    getUniqueUsername([...alternativeUsernames, ...usernames]),
                );
              }

              const res = StatusDto(
                  false,
                  ErrorDto(
                      error,
                      {
                        alternatives: alternativeUsernames,
                      }),
              );

              return response.
                  status(400).
                  json(res);

            }

            return response.
                status(400).
                json(StatusDto(false, ErrorDto(error)));
          };

  private handleCreateUserError =
      (request: Request, response: Response) =>
          async (error: Error) => {

            const user: user.models.NewUserPayload = request.body;

            if (error instanceof UserExistsError) {

              const alternativeUsername = await UserService.getNewUsername(
                  user.username,
              );
              const res = StatusDto(
                  false,
                  ErrorDto(
                      error,
                      {
                        alternatives: alternativeUsername ? [
                          alternativeUsername,
                        ] : undefined,
                      }),
              );

              return response.
                  status(400).
                  json(res);

            }

            const res = StatusDto(false, ErrorDto(error));
            return response.
                status(400).
                json(res);

          };

};
