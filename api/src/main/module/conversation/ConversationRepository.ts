import {selectFirst} from '../database/utils';
import {connection} from '../database';
import UserRepository from '../user/UserRepository';
import {
  ConversationCreationError,
  ConversationUpdateError,
  NoConversationFoundError,
} from './errors';
import {MessageDto} from './response';

export default new class ConversationRepository {
  ConversationTable = () => connection('user_conversation');
  MessageTable = () => connection('message');

  async findUserConversationById(userId: number, conversationId: number) {

    const getSubQuery = () => this.ConversationTable().
        where({id: conversationId}).
        where('user_1', userId).
        orWhere('user_2', userId);

    const messages = await this.MessageTable().
        where('user_conversation', '=', conversationId).
        whereExists(
            getSubQuery().whereRaw(
                'message.user_conversation = user_conversation.id'),
        ).
        orderBy('created_at').
        select();

    return getSubQuery().
        where('id', '=', conversationId).
        select().
        then(selectFirst).
        then(conversation => {
          if (conversation) {
            return {
              ...conversation,
              messages,
            };
          }

          return Promise.reject(new NoConversationFoundError());
        });

  }

  async findUserConversations(userId: number) {
    // maybe think about either moving to nosql or caching this

    const conversations: conversation.models.Conversation[] = await this.ConversationTable().
        where('user_1', userId).
        orWhere('user_2', userId).
        select();

    const conversationIds = conversations.map(({id}) => id);
    const messages: conversation.models.Message[] = await this.MessageTable().
        whereIn(
            'user_conversation',
            conversationIds,
        ).groupBy('user_conversation').
        select('*');

    return conversations.map(c => {

      c.last_message = MessageDto(messages.find(
          message => message.user_conversation === c.id));

      return c;

    });

  }

  createConversation(
      senderUserId,
      {recipientUserId}: conversation.models.NewConversationPayload) {

    const findExistingConversation = this.ConversationTable().
        where(function() {
          this.
              where('user_1', senderUserId).
              orWhere('user_2', senderUserId);
        }).
        andWhere(function() {
          this.
              where('user_1', recipientUserId).
              orWhere('user_2', recipientUserId);
        }).select();

    return findExistingConversation.then(rows => {
      if (!rows || rows.length === 0) {
        return UserRepository.findUserById(recipientUserId).
            then(user => {
              if (user) {
                return this.ConversationTable().insert({
                  user_1: senderUserId,
                  user_2: recipientUserId,
                  created_at: new Date(),
                  updated_at: new Date(),
                });
              }
              return Promise.reject(new ConversationCreationError());
            }).then(rows => rows[0]);
      }

      return rows[0].id;
    });

  }

  updateConversation(
      userId: number, payload: conversation.models.UpdateConversationPayload) {
    const {
      conversationId,
      message,
    } = payload;

    if(!message) {
      return Promise.reject(new ConversationUpdateError());
    }

    return this.ConversationTable().
        where({id: conversationId}).
        then(selectFirst).
        then(conversation => {
          if (conversation) {
            return this.MessageTable().insert({
              user_conversation: conversationId,
              user: userId,
              message: message,
              created_at: new Date(),
              updated_at: new Date(),
            }).then(async (rows) => {
              await this.ConversationTable().where({
                id: conversationId,
              }).update({
                updated_at: new Date(),
              });
              return rows[0];
            }).catch(() => new ConversationUpdateError());
          }

          return Promise.reject(new NoConversationFoundError());
        });

  }

  delete(
      userId: number, payload: conversation.models.DeleteConversationPayload) {
    const {
      conversationId,
    } = payload;
    const USER_1_FIELD = 'user_1';
    const USER_2_FIELD = 'user_2';
    return this.ConversationTable().
        where({id: conversationId}).
        where(USER_1_FIELD, userId).
        orWhere(USER_2_FIELD, userId).
        select().
        then(selectFirst).
        then(res => {
          if (res) {

            const key = userId === res.user_1
                ? USER_1_FIELD
                : USER_2_FIELD;

            return this.ConversationTable().where({
              id: conversationId,
            }).update({
              [key]: null,
              updated_at: new Date(),
            });
          }
        });
  }
};