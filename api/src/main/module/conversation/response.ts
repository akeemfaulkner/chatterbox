import {UserDto} from '../user/response';

export function MessageDto(data?: conversation.models.Message) {

  if (!data) {
    return null;
  }

  const {id, message, created_at, user} = data;

  return {
    id,
    message,
    created_at,
    user,
  };
}

export function ConversationDto(data: conversation.models.Conversation & {
  messages: conversation.models.Message[]
}, users: user.models.User[]) {

  const {
    id,
    created_at,
    updated_at,
    messages,
    user_1,
    user_2,
  } = data;

  return {
    id,
    created_at,
    updated_at,
    user_1: users.find(user => user.id === user_1),
    user_2: users.find(user => user.id === user_2),
    messages: messages.map(MessageDto),
  };
}

export function ConversationListDto(
    conversations: conversation.models.Conversation[],
    users: user.models.User[]) {

  const userMap = {};
  users.forEach(user => {
    userMap[user.id] = user;
  });

  return conversations.map((conversation) => {
    const userId1 = conversation.user_1;
    const userId2 = conversation.user_2;

    const lastMessageUserId = conversation.last_message ?
        conversation.last_message.user :
        null;

    return {
      id: conversation.id,
      user_1: UserDto(userMap[userId1]),
      user_2: UserDto(userMap[userId2]),
      created_at: conversation.created_at,
      last_message: lastMessageUserId ? {
        ...conversation.last_message,
        user: UserDto(userMap[lastMessageUserId]),
      } : null,
      updated_at: conversation.updated_at,
    };
  });
}