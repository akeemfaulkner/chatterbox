import ConversationRepository from './ConversationRepository';

export default new class ConversationService {

  getConversation(userId: number, conversationId: number) {
    return ConversationRepository.findUserConversationById(userId, conversationId);
  }

  getUserConversations(userId: number) {
    return ConversationRepository.findUserConversations(userId);
  }

  create(senderUserId, payload: conversation.models.NewConversationPayload) {
    return ConversationRepository.createConversation(senderUserId, payload);
  }

  update(userId: number, payload: conversation.models.UpdateConversationPayload) {
    return ConversationRepository.updateConversation(userId, payload);
  }

  delete(
      userId: number,
      payload: conversation.models.DeleteConversationPayload,
  ) {
    return ConversationRepository.delete(userId, payload);
  }
};