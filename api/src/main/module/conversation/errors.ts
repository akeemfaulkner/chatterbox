import {DomainError} from '../../common/errors';

export class NoConversationFoundError extends DomainError {
  message = 'No conversation was found';
  name = 'NoConversationFoundError';
}

export class ConversationCreationError extends DomainError {
  message = 'There was an error creating a conversation';
  name = 'ConversationCreationError';
}

export class ConversationUpdateError extends DomainError {
  message = 'There was an error updating a conversation';
  name = 'ConversationUpdateError';
}