import {Request, Response} from 'express';
import ConversationService from './ConversationService';
import {ErrorDto, StatusDto} from '../../common/response';
import {ConversationListDto, ConversationDto} from './response';
import UserRepository from '../user/UserRepository';
import {UserDto} from '../user/response';
import {NoConversationFoundError} from './errors';

export default new class UserController {

  get(request: Request, response: Response) {
    const user = request.user;
    return ConversationService.
        getConversation(user.id, request.params.id).
        then(async result => {
          const users: user.models.User[] = await UserRepository.UserTable().
              whereIn(
                  'id',
                  [result.user_1, result.user_2],
              ).select();
          response.json(ConversationDto(result, users))
        }).
        catch(error => {
          return response.
              status(404).
              json(StatusDto(false, ErrorDto(error)));
        });

  }

  getUserConversations(request: Request, response: Response) {
    const user = request.user;
    return ConversationService.
        getUserConversations(user.id).
        then(async (conversations: conversation.models.Conversation[]) => {

          const userIdMap = conversations.map(({user_1, user_2}) => ({
            [user_1]: user_1,
            [user_2]: user_2,
          })).reduce((prev, cur) => ({...prev, ...cur}), {});

          const users: user.models.User[] = await UserRepository.UserTable().
              whereIn(
                  'id',
                  Object.values(userIdMap),
              ).select();

          if (conversations) {
            return ConversationListDto(conversations, users);
          }

          return Promise.reject(new NoConversationFoundError());
        }).
        then(result => response.json(result)).
        catch(error => {
          return response.
              status(404).
              json(StatusDto(false, ErrorDto(error)));
        });

  }

  delete(request: Request, response: Response) {
    const user = request.user;
    return ConversationService.delete(user!.id,
        {conversationId: request.params.id}).
        then(() => response.
            status(204).
            json(StatusDto(true)),
        ).
        catch(error => {
          return response.
              status(400).
              json(StatusDto(false, ErrorDto(error)));
        });

  }

  create(request: Request, response: Response) {
    const user = request.user;
    return ConversationService.create(user.id, request.body).
        then((conversationId) => {
          response.json(StatusDto(true, {conversation: conversationId}));
        }).
        catch(error => {
          return response.
              status(400).
              json(StatusDto(false, ErrorDto(error)));
        });
  }


};