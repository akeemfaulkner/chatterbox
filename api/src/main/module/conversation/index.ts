import {Application} from 'express';
import ConversationController from './ConversationController';
import {authenticatedRoute} from '../authentication';
import {AUTH_TOKEN_COOKIE_NAME} from '../../../../../common/constants';
import {parseCookieFromString} from '../../../../../common/utils';
import AuthenticationService from '../authentication/AuthenticationService';
import UserRepository from '../user/UserRepository';
import {UserDto} from '../user/response';
import {MessageDto} from './response';
import ConversationService from './ConversationService';

const withController = (app: Application) => {
  app.get(
      '/conversation',
      authenticatedRoute,
      ConversationController.getUserConversations,
  );

  app.get(
      '/conversation/:id',
      authenticatedRoute,
      ConversationController.get,
  );

  app.post(
      '/conversation',
      authenticatedRoute,
      ConversationController.create,
  );

  app.delete(
      '/conversation/:id',
      authenticatedRoute,
      ConversationController.delete,
  );

  return app;
};

export const chatWebsocket = app => {

  const io = require('socket.io')(app);

  io.use(async (socket, next) => {

    const cookies = socket.handshake.headers.cookie;

    const token = parseCookieFromString(
        cookies,
        AUTH_TOKEN_COOKIE_NAME,
    );

    try {
      const {id} = await AuthenticationService.decodeJWTToken(token);

      if (id) {
        const user = await UserRepository.findUserById(id);
        socket.user = UserDto(user);
        next();
      }
    } catch (e) {
      next(new Error('Authentication error'));
    }

  });

  const users = {};

  io.on('connection', function(socket) {
    const user = socket.user;

    if (!user) {
      return;
    }

    users[user.id] = socket.id;

    socket.on('start_conversation', function({recipientId, conversationId}) {
      const socketId = users[recipientId];
      if (socketId) {
        io.to(socketId).
            emit('join_conversation', {conversation: conversationId, user});
        socket.join(`conversation-${conversationId}`);
      }
    });

    socket.on('join_conversation', function({conversationId}) {
      if (conversationId) {
        socket.join(`conversation-${conversationId}`);
      }
    });

    socket.on('sending_message', function({conversationId}) {
      const room = `conversation-${conversationId}`;
      socket.to(room).
          emit('updating_message');
    });

    socket.on('send_message', function({conversationId, message}) {
      const rooms = Object.keys(socket.rooms);
      const room = `conversation-${conversationId}`;
console.log(conversationId, message, rooms)
      if(rooms.includes(room)) {

        ConversationService.update(
            user.id,
            {
              message,
              conversationId: conversationId,
            },
        ).
            then((id) => {

              io.in(room).
                  emit('new_message', MessageDto({
                    id: id,
                    message,
                    created_at: Date.now(),
                    updated_at: Date.now(),
                    user: user.id,
                    user_conversation: conversationId
                  }));
            })



      }

    });

  });

  return app;
};

export const conversation = withController;

