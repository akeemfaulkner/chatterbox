declare namespace Express {
  export interface Request {
    user: user.models.User;
    fingerprint: object;
    files: any[];
    file: any;
  }
}

declare namespace Knex {

  export interface QueryInterface {
    paginate: Paginate;
  }

  export interface Paginate {
    (current_page: number, per_page: number): PaginationResponse;
  }

  export interface PaginationResponse {
    total: number;
    per_page: number;
    offset: number;
    to: number;
    last_page: number;
    current_page: number;
    from: number;
    data: any[];
  }
}

declare namespace user.models {

  export interface User {
    id: number;
    username: string;
    email: string;
    password: string;
    bio: string;
    profile_photo: string;
  }

  export type NewUserPayload = Pick<User,
      | 'username'
      | 'email'
      | 'password'>

  export type DeleteUserPayload = {
    id: number;
  }

  export type NewUsernamePayload = {
    id: number;
    new_username: string;
    old_username: string;
  }
}

declare namespace conversation.models {

  export interface Message {
    id: number;
    user: number;
    user_conversation: number;
    message: string,
    created_at: number;
    updated_at: number;
  }

  export interface Conversation {
    id: number;
    user_1: number;
    user_2: number;
    created_at: number;
    updated_at: number;
    last_message?: Partial<Message> | null;
  }

  export interface NewConversationPayload {
    recipientUserId: number;
  }

  export interface UpdateConversationPayload {
    conversationId: number;
    message: number;
  }

  export interface DeleteConversationPayload {
    conversationId: number;
  }
}

declare namespace authentication.models {

  export interface JWTToken {
    id: number;
    fingerprint: string;
  }

  export interface AuthenticationPayload {
    username: string;
    password: string;
  }
}