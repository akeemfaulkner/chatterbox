import app from './app';
import {chatWebsocket} from './module/conversation';

const server = require('http').Server(app);


export default chatWebsocket(server);