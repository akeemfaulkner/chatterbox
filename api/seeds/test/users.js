const bcrypt = require('bcrypt');

const users = [
  {
    'username': 'test_username',
    'first_name': 'test_firstName',
    'last_name': 'test_lastName',
    'email': 'email@email.com',
    'password': 'password123',
  },
  {
    'username': 'test2_username',
    'first_name': 'test2_firstName',
    'last_name': 'test2_lastName',
    'email': 'email2@email.com',
    'password': 'password1233',
  },
];

module.exports.users = users;

exports.seed =  function(knex, Promise) {
  // Deletes ALL existing entries

  const data = JSON.parse(JSON.stringify(users));

  return knex('users').del().then(async function() {

    for (let i = 0; i < data.length; i++) {
      const user = data[i];
      user.password = await bcrypt.hash(user.password, 10);
    }
    return knex('users').insert(data);
  });
};
