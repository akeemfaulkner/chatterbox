import {connection} from '../../src/main/module/database';

beforeAll(async () => {
  const connection = require('knex')(
      require('../../knexfile')[process.env.NODE_ENV]);

  try {
    await connection.migrate.latest();
    await connection.seed.run();

    console.log('Global Test Setup complete!');

  } catch (e) {
    console.log(e);
  } finally {
    connection.destroy();
  }
});

afterAll(() => {
  return connection.destroy(() => console.info('Closing database connection.'));
});