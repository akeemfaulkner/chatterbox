exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('users', function(table) {

      table.increments('id').primary();
      table.string('username').unique();
      table.string('email').unique();
      table.string('password');
      table.string('profile_photo');
      table.text('bio');
      table.timestamps();
      table.index(['username']);
    }),

    knex.schema.createTable('user_conversation', function(table) {
      table.increments('id').primary();

      table.integer('user_1').unsigned();
      table.integer('user_2').unsigned();

      table.index(['user_1', 'user_2']);
      table.foreign('user_1').references('id').inTable('user');
      table.foreign('user_2').references('id').inTable('user');
      table.timestamps();
    }),

    knex.schema.createTable('message', function(table) {
      table.increments('id').primary();

      table.integer('user').unsigned().notNullable();
      table.integer('user_conversation').unsigned().notNullable();
      table.boolean('is_read').defaultTo(false);
      table.foreign('user').references('id').inTable('user');
      table.foreign('user_conversation').references('id').inTable('user_conversation');
      table.text('message');
      table.timestamps();
    }),
  ]);

};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('users'),
    knex.schema.dropTable('user_conversation'),
    knex.schema.dropTable('message'),
  ]);
};
