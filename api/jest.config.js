module.exports = {
  "roots": [
    "<rootDir>/src"
  ],
  "transform": {
    "^.+\\.ts?$": "ts-jest"
  },
  "testRegex": "((\\.|/)test)\\.ts$",
  "collectCoverageFrom":[
    "<rootDir>/src/main/**/*.ts"
  ],
  "moduleFileExtensions": [
    "ts",
    "js",
    "json",
    "node"
  ],
  "setupTestFrameworkScriptFile": "<rootDir>/config/jest/setupTestFrameworkScriptFile.ts",
  "bail": true,
  "testEnvironment": "node"
}