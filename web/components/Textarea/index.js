import React from 'react';
import './styles.scss';

const Textarea = (props) => <textarea className="Textarea" {...props}/>;

export default Textarea;
