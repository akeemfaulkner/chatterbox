import React, {Component} from 'react';
import './styles.scss';
import Login from '../Login';
import Signup from '../Signup';

const FormType = {
  LOGIN: 'LOGIN',
  SIGN_UP: 'SIGN_UP',
};

class Authentication extends Component {

  state = {
    form: FormType.LOGIN,
    formClassName: 'Authentication__form',
  };

  render() {

    return (
        <div className="Authentication">
          {this.renderForm()}
        </div>
    );
  }

  renderForm() {

    const {formClassName} = this.state;

    const isLoginForm = this.isCurrentFormType(FormType.LOGIN);

    const Form = isLoginForm
        ? Login
        : Signup;

    return (
        <Form
            onFormLink={this.onFormLink}
            className={formClassName}
        />
    );
  }

  isCurrentFormType(formType) {
    return this.state.form === formType;
  }

  onFormLink = () => {

    const isLoginForm = this.isCurrentFormType(FormType.LOGIN);

    const form = isLoginForm
        ? FormType.SIGN_UP
        : FormType.LOGIN;

    this.setState({
      form,
      formClassName: 'Authentication__form--active',
    });
  };
}

export default Authentication;