import React, {Component} from 'react';
import InputGroup from '../InputGroup';
import './styles.scss';
import Form from '../Form';
import {InputField, withSettingsForm} from '../../core/lib/redux-form';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Img from '../Img';

class Settings extends Component {

  state = {
    fileError: null,
  };

  render() {
    const {handleSubmit, error, profile} = this.props;
    return (
        <div className="Settings">
          <Form
              buttonText="Save"
              header="Update Settings"
              onSubmit={handleSubmit}
              error={error}
          >
            {profile.profile_photo && (
                <div className="ProfilePreview__thumbnail">
                  <Img src={profile.profile_photo}/>
                </div>
            )}

            <InputGroup
                type="file"
                label="Profile Photo"
                error={this.state.fileError}
                onChange={this.handleFileChange}
            />
            <InputField
                label="Username"
                name="username"
            />
            <InputField
                type="textarea"
                label="Bio"
                name="bio"
                maxLength={250}
            />
          </Form>
        </div>
    );
  }

  handleFileChange = event => {
    event.persist();
    const {change} = this.props;
    const files = event.target.files;
    const file = files[0];

    if (file.type.match('image.*')) {
      this.setState({fileError: null});
      change('profile_photo', file);
    } else {
      this.setState({fileError: 'Invalid file type'});
    }
  };
}

export default compose(
    connect(({profile}) => {
      return {
        profile,
        initialValues: profile,
      };
    }),
    withSettingsForm,
)(Settings);