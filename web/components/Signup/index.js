import React, {Component} from 'react';
import Form from '../Form';
import classNames from 'classnames';
import {InputField, withSignUpForm} from '../../core/lib/redux-form';


class Signup extends Component {

  render() {

    const {className, onFormLink, handleSubmit, error} = this.props;

    return (
        <div className={classNames('Signup', className)}>

          <Form
              buttonText="Sign up!"
              header="Sign up"
              onSubmit={handleSubmit}
              error={error}
          >

            <InputField
                label="Username"
                name="username"
            />

            <InputField
                label="Email"
                type="email"
                name="email"
            />

            <InputField
                label="Password"
                type="password"
                name="password"
            />

            <InputField
                label="Confirm Password"
                type="password"
                name="confirm_password"
            />

            <a className="Form__link" onClick={onFormLink}>
              Already have an account?
            </a>

          </Form>

        </div>
    );
  }
}

export default withSignUpForm(Signup);
