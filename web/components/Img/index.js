import React, {Component} from 'react';
import http from '../../core/http';
import './styles.scss';

class Img extends Component {
  state = {
    src: undefined,
    width: undefined,
    height: undefined,
  };

  render() {
    const {width, src, height} = this.state;
    return (
       <div className="Img" style={{width, height: width}}>
         <img
             src={src}
             width={width}
             height={height}
         />
       </div>
    );
  }

  componentDidUpdate(lastProps) {

    if (lastProps.src !== this.props.src) {
      this.loadImage();
    }
  }

  componentDidMount() {
    this.loadImage();
  }

  loadImage() {
    const src = this.props.src;

    if (!src) {
      return null;
    }

    http.get(src, {
      responseType: 'blob',
    }).then(({data}) => {
      const image = new Image();
      const objectURL = URL.createObjectURL(data);
      image.src = objectURL;
      image.onload = () => {
        const {width, height} = image;
        this.setState({src: objectURL, width, height});
      };

    });
  }
}

export default Img;