import classNames from 'classnames';
import React, {Component} from 'react';
import './styles.scss';
import Form from '../Form';
import {InputField, withLoginForm} from '../../core/lib/redux-form';

class Login extends Component {
  render() {
    const {className, onFormLink, handleSubmit, error} = this.props;
    return (
        <div className={classNames('Login', className)}>
          <Form
              buttonText="Login!"
              header="Login"
              onSubmit={handleSubmit}
              error={error}
          >

            <InputField
                label="Username"
                name="username"
            />

            <InputField
                label="Password"
                type="password"
                name="password"
            />

            <a className="Form__link" onClick={onFormLink}>
              Don't have an account?
            </a>

          </Form>
        </div>
    );
  }
}

export default withLoginForm(Login);
