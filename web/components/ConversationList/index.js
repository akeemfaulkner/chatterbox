import React, {Component} from 'react';
import './styles.scss';
import {connect} from 'react-redux';
import {getConversations} from '../../core/state/conversation/actionCreators';
import {selectConversation, selectCurrentUser} from '../../core/selectors';
import Message from '../Message';
import {withRouter} from 'next/router';
import ContactFinder from '../ContactFinder';

/*
 * Easing Functions - inspired from http://gizma.com/easing/
 * only considering the t value for the range [0, 1] => [0, 1]
 */
const EasingFunctions = {
  // no easing, no acceleration
  linear: function(t) {
    return t;
  },
  // accelerating from zero velocity
  easeInQuad: function(t) {
    return t * t;
  },
  // decelerating to zero velocity
  easeOutQuad: function(t) {
    return t * (2 - t);
  },
  // acceleration until halfway, then deceleration
  easeInOutQuad: function(t) {
    return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
  },
  // accelerating from zero velocity
  easeInCubic: function(t) {
    return t * t * t;
  },
  // decelerating to zero velocity
  easeOutCubic: function(t) {
    return (--t) * t * t + 1;
  },
  // acceleration until halfway, then deceleration
  easeInOutCubic: function(t) {
    return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
  },
  // accelerating from zero velocity
  easeInQuart: function(t) {
    return t * t * t * t;
  },
  // decelerating to zero velocity
  easeOutQuart: function(t) {
    return 1 - (--t) * t * t * t;
  },
  // acceleration until halfway, then deceleration
  easeInOutQuart: function(t) {
    return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
  },
  // accelerating from zero velocity
  easeInQuint: function(t) {
    return t * t * t * t * t;
  },
  // decelerating to zero velocity
  easeOutQuint: function(t) {
    return 1 + (--t) * t * t * t * t;
  },
  // acceleration until halfway, then deceleration
  easeInOutQuint: function(t) {
    return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
  },
};

class ConversationList extends Component {
  root = React.createRef();
  resizer = React.createRef();

  render() {

    const {conversation} = this.props;

    return (
        <div className="ConversationList" ref={this.root}>
          <div className="ConversationList__inner">
            <ContactFinder/>
            {
              !conversation.list
                  ? 'Loading...'
                  : this.renderConversationList()
            }
          </div>
          <div className="ConversationList__resizer" ref={this.resizer} draggable={true}/>
        </div>
    );
  }

  renderConversationList() {
    const {conversation, profile} = this.props;

    return conversation.list.map(
        (conversation) => {

          const {user_1, user_2, id} = conversation;
          const lastMessage = conversation.last_message || {};
          const user = profile.id === user_2.id ? user_1 : user_2;
          const message = {...lastMessage, user};
          const href = `#/conversation=${id}`;

          return (
              <a
                  key={id}
                  className="ConversationList__item"
                  href={href}
              >
                <Message
                    message={message}
                    truncated={true}
                />
              </a>
          );
        });
  }

  componentDidMount() {
    this.props.getConversations();

    const el = this.root.current;
    const drag = (e) => {
      if (active) {

        e.preventDefault();

        if (e.type === 'touchmove') {
          currentX = e.touches[0].clientX - initialX;
          currentY = e.touches[0].clientY - initialY;
        } else {
          currentX = e.clientX - initialX;
          currentY = e.clientY - initialY;
        }

        xOffset = currentX;
        yOffset = currentY;

        setTranslate(currentX, currentY, this.root.current);
      }
    };

    var active = false;
    var isOpening = false;
    var currentX;
    var currentY;
    var initialX;
    var initialY;
    var xOffset = 0;
    var yOffset = 0;

    function dragStart(e) {

      const {width} = el.getBoundingClientRect();
      isOpening = width === 0;

      if (e.type === 'touchstart') {
        initialX = e.touches[0].clientX - xOffset;
        initialY = e.touches[0].clientY - yOffset;
      } else {
        initialX = e.clientX - xOffset;
        initialY = e.clientY - yOffset;
      }
      active = true;
    }

    function dragEnd(e) {
      setTimeout(() => {
        let width = 250 + currentX;
        if (width < 250 - 100) {
          el.style.width = 0;
        } else {
          el.style.width = '250px';
          xOffset = 0;
          yOffset = 0;
        }
      }, 300);

      initialX = currentX;
      initialY = currentY;
      active = false;
    }

    function interpolate(
        from, to, time, easing = EasingFunctions.easeInOutQuad) {
      const easedTime = easing(time);
      return (1 - easedTime) * from + easedTime * to;
    }

    function setTranslate(xPos, yPos, el) {
      const maxWidth = 250;
      let width = maxWidth + xPos;
      width = width > 250 ? maxWidth : width;
      const unclampedDelta = 1 - (width / 250);
      const delta = 1 - Math.min(1, Math.max(0, unclampedDelta));
      el.style.width = interpolate(0, 250, delta) + 'px';
    }

    this.resizer.current.addEventListener('mousedown', dragStart, false);
    document.addEventListener('mouseup', dragEnd, false);
    document.addEventListener('mouseleave', dragEnd, false);
    document.addEventListener('mousemove', drag, false);
  }

}

ConversationList = withRouter(ConversationList);

export default connect(
    state => ({
      ...selectConversation(state),
      ...selectCurrentUser(state),
    }),
    {
      getConversations,
    },
)(ConversationList);