import React, {Component} from 'react';
import './styles.scss';
import Button from '../Button';
import Alert from '../Alert';
import Card from '../Card';

class Form extends Component {
  render() {
    const {children, buttonText, header, onSubmit, error} = this.props;
    return (
        <Card header={header}>
          <form method="post" className="Form" onSubmit={onSubmit}>
            <div className="Form__fields">
              <Alert>
                {error}
              </Alert>
              {children}
              <Button type="submit">{buttonText}</Button>
            </div>
          </form>
        </Card>
    );
  }
}

export default Form;