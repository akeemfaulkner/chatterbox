import React from 'react';
import './styles.scss';

export default function Card({children, header}) {
  return (
      <div className="Card" >

        {header && (
            <h3 className="Card__header">{header}</h3>
        )}

        {children}
      </div>
  );
}