import React, {Component} from 'react';
import './styles.scss';


class IntroLoader extends Component {

  render() {
    return (
        <div className="IntroLoader">

          <h1 className="IntroLoader__content">
            Chatterbox
          </h1>

        </div>
    );
  }

  componentDidMount = () => {

   setTimeout(()=>{
     const onComplete = this.props.onComplete;
     if (onComplete) {
       onComplete();
     }
   }, 3200)
  };

}

export default IntroLoader;