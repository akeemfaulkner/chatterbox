import React, {Component} from 'react';
import _ from 'lodash';
import Message from '../Message';
import classNames from 'classnames';
import {
  hasBothHistoriesSet,
  historyIdsAreTheSame,
  messagedHaveUpdated,
} from './utils';
import {connect} from 'react-redux';
import {selectCurrentUser} from '../../core/selectors';
import './styles.scss';

class Messages extends Component {
  messagesRef = React.createRef();
  state = {
    scrolledToBottom: false,
  };

  render() {
    const {conversation: {history}} = this.props;

    return (
        <div className="Messages" ref={this.messagesRef}>
          {_.get(history, 'messages', []).map(message => {
            const user = this.getUser(message.user);
            return (
                <div
                    className={this.getMessageClassName(user)}
                    key={message.id}
                >
                  <Message
                      message={{
                        ...message,
                        user,
                      }}
                  />
                </div>
            );
          })}
        </div>
    );
  }

  componentDidUpdate(lastProps) {
    this.scrollToBottomOnDidUpdate(lastProps);
  }

  getMessageClassName(user) {
    const isCurrentUser = this.props.isCurrentUser(user);
    return classNames(
        'Messages__message', {
          'Messages__message--current': isCurrentUser,
        });
  }

  scrollToBottomOnDidUpdate(lastProps) {

    const {conversation: {history}} = this.props;
    const historiesSet = hasBothHistoriesSet(lastProps, this.props);
    const currentMessages = _.get(history, 'messages', []);

    if (historiesSet) {

      if (messagedHaveUpdated(lastProps, this.props)) {
        this.scrollMessagesToBottom();
      }

      if (!historyIdsAreTheSame(lastProps, this.props)) {
        this.setState(
            {scrolledToBottom: false},
            this.scrollMessagesToBottom,
        );
      }

    } else if (!this.state.scrolledToBottom && currentMessages.length) {
      this.setState(
          {scrolledToBottom: true},
          this.scrollMessagesToBottom,
      );
    }

  }

  scrollMessagesToBottom = () => {
    const element = this.messagesRef.current;
    element.scrollTop = element.scrollHeight;
  };

  getUser(userId) {

    const {conversation: {history}} = this.props;

    if (!history) {
      return null;
    }

    const isUser1 = history.user_1.id === userId;
    return isUser1 ? history.user_1 : history.user_2;
  }

}

export default connect(
    state => ({
      ...selectCurrentUser(state),
    }),
)(Messages);