import _ from 'lodash';

export const hasBothHistoriesSet = (lastProps, props) => {
  const lastHistory = _.get(lastProps.conversation, 'history');
  const currentHistory = _.get(props.conversation, 'history');
  return lastHistory && currentHistory;
};

export const historyIdsAreTheSame = (lastProps, props) => {
  const lastHistoryId = _.get(lastProps.conversation, 'history.id');
  const currentHistoryId = _.get(props.conversation, 'history.id');
  return lastHistoryId === currentHistoryId;
};

export const messagedHaveUpdated = (lastProps, props) => {
  const lastMessages = _.get(lastProps.conversation, 'history.messages', []);
  const currentMessages = _.get(props.conversation, 'history.messages', []);
  return lastMessages.length !== currentMessages.length;
};


