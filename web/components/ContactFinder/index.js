import React, {Component} from 'react';
import _ from 'lodash';
import InputGroup from '../InputGroup';
import './styles.scss';
import {connect} from 'react-redux';
import {clearUsers, findUsers} from '../../core/state/users/actionCreators';
import {startConversation} from '../../core/state/conversation/actionCreators';

class ContactFinder extends Component {
  render() {
    const {users, clearUsers} = this.props;
    return (
        <div className="ContactFinder">

          <InputGroup label="Find Contact" onChange={this.onChange}/>

          <div className="ContactFinder__results" onMouseLeave={clearUsers}>
            {users && users.data.map((user) => {
              return (
                  <div
                      className="ContactFinder__contact"
                      key={user.id}
                      onClick={() => this.startConversation(user.id)}
                  >
                    <b>{user.username}</b>
                    <div>{user.email}</div>
                  </div>
              );
            })}
          </div>
        </div>
    );
  }

  startConversation = (id) => {
    this.props.startConversation(id);
    this.props.clearUsers();
  };

  onChange = (e) => {
    const value = e.target.value;
    this.findContacts(value);
  };

  findContacts = _.debounce((value) => {
    this.props.findUsers(value);
  }, 300);
}

export default connect(({users: {results}}) => ({
  users: results,
}), {findUsers, clearUsers, startConversation})(ContactFinder);