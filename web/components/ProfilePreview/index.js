import React, {Component} from 'react';
import './styles.scss';
import Img from '../Img';
import {connect} from 'react-redux';
import {selectCurrentUser} from '../../core/selectors';

class ProfilePreview extends Component {
  render() {
    const {profile} = this.props;
    const bio = (profile.bio || '').replace(/(?:\r\n|\r|\n)/g, '<br>');
    return (
        <div className="ProfilePreview">
          <h3 className="ProfilePreview__username">{profile.username}</h3>

          {profile.profile_photo && (
              <div className="ProfilePreview__thumbnail">
                <Img src={profile.profile_photo}/>
              </div>
          )}

          {
            bio && (
                <>
                  <b className="ProfilePreview__header">Bio:</b>
                  <p className="ProfilePreview__bio"
                     dangerouslySetInnerHTML={{__html: bio}}/>
                </>
            )
          }
        </div>
    );
  }
}

export default connect(
    selectCurrentUser,
)(ProfilePreview);