import React, {Component} from 'react';
import './styles.scss';
import Settings from '../Settings';

class Toolbar extends Component {
  state = {
    displaySettings: false,
  };

  render() {
    const {logout} = this.props;
    return (
        <div className="Toolbar">
          <button className="Toolbar__settings" onClick={this.toggleDisplaySettings}/>
          <button className="Toolbar__logout" onClick={logout}/>
          {this.state.displaySettings && (
              <Settings onSave={this.toggleDisplaySettings}/>
          )}
        </div>
    );
  }

  toggleDisplaySettings = () => {
    this.setState({displaySettings: !this.state.displaySettings});
  };
}

export default Toolbar;