import React from 'react';
import classNames from 'classnames';
import './styles.scss';
import {unixTimestampToDate} from '../../core/utils';

const Message = ({message, truncated}) => {
  const className = classNames('Message', {'Message--truncated': truncated});
  return (
      <div className={className}>
        <div className="Message__header">
          <b>{message.user.username}</b>
          <div>
            {
              message.created_at ?
                  unixTimestampToDate(message.created_at) :
                  null
            }
          </div>
        </div>
        <p className="Message__message">
          {getMessage(message)}
        </p>
      </div>
  );
};

function getMessage(message) {
  return message && message.message
      ? message.message
      : '';
}

export default Message;
