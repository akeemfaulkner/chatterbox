import React, {Component} from 'react';
import './styles.scss';
import Textarea from '../Textarea';

class InputGroup extends Component {
  render() {

    const {label, type = 'text', error, ...rest} = this.props;
    const isTextArea = type === 'textarea';

    return (
        <div className="InputGroup">
          <label>{label}</label>

          {isTextArea
              ? <Textarea {...rest}/>
              : <input type={type} {...rest}/>
          }

          <small
              className="InputGroup__error"
              dangerouslySetInnerHTML={{__html: error || '&nbsp;'}}
          />
        </div>
    );
  }
}

export default InputGroup;