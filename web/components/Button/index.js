import React from 'react';
import './styles.scss';

export default function Button({children, type = 'button', ...rest}) {
  return (
      <button className="Button" type={type} {...rest}>
        <div className="Button__content">{children}</div>
      </button>
  );
}