import React, {Component} from 'react';
import './styles.scss';
import ConversationList from '../ConversationList';
import Conversation from '../Conversation';
import ProfilePreview from '../ProfilePreview';
import {connect} from 'react-redux';
import {selectConversation} from '../../core/selectors';

class Dashboard extends Component {
  render() {
    const {conversation: {selectedConversationId}}= this.props;
    return (
        <div className="Dashboard">
          <ConversationList/>
          <Conversation conversationId={selectedConversationId}/>
          <ProfilePreview/>
        </div>
    );
  }
}

export default connect(
    state => ({
      ...selectConversation(state)
    })
)(Dashboard);