import React, {Component} from 'react';
import _ from 'lodash';
import './styles.scss';
import Button from '../Button';
import {connect} from 'react-redux';
import {
  joinConversation,
  sendMessage, updatingMessage,
} from '../../core/state/conversation/actionCreators';
import {selectConversation} from '../../core/selectors';
import Messages from '../Messages';
import {hashUrlToObject} from '../../core/utils';

class Conversation extends Component {

  state = {
    message: '',
  };

  render() {
    const {conversation} = this.props;
    const {message} = this.state;

    return (
        <div className="Conversation">
          <Messages conversation={conversation}/>
          <div className="Conversation__text-box">
            {this.renderUpdate()}
            <textarea
                onChange={this.sendUpdatingMessage}
                value={message}
            />
            <Button onClick={this.sendMessage}>
              Send
            </Button>
          </div>
        </div>
    );
  }

  sendUpdatingMessage = event => {
    const conversationId = this.getConversationId();
    if (conversationId && event.target.value) {
      this.setState(
          {message: event.target.value},
          () => updatingMessage(conversationId),
      );
    }
  };

  sendMessage = () => {
    const {message} = this.state;
    const conversationId = this.getConversationId();

    if (conversationId && message) {
      this.props.sendMessage(conversationId, message);
    }

  };

  renderUpdate() {
    const {history} = this.props.conversation;
    return _.get(history, 'updating') && (
        <small className="Conversation__update">
          {this.getOtherUser()} is typing...
        </small>
    );
  }

  getOtherUser() {

    const {conversation: {history}, profile} = this.props;

    if (history && profile) {
      const isUser1 = profile.id === history.user_1.id;
      const user = isUser1 ? history.user_2 : history.user_1;
      return user.username;
    }

    return null;
  }

  componentDidMount() {
    this.joinConversation();
    window.addEventListener('hashchange', this.joinConversation);
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.joinConversation);
  }

  joinConversation = () => {
    const {joinConversation} = this.props;
    const conversationId = this.getConversationId();

    if (conversationId) {
      joinConversation(conversationId);
    }

  };

  getConversationId() {
    return hashUrlToObject()['conversation'];
  }

}

export default connect(
    selectConversation,
    {
      sendMessage,
      updatingMessage,
      joinConversation,
    },
)(Conversation);