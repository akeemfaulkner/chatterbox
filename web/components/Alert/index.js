import React from 'react';
import './styles.scss';

const Alert = ({children}) => children
    ? (
        <div className="Alert">
          {children}
        </div>
    )
    : null;

export default Alert;
