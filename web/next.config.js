const withSass = require('@zeit/next-sass');

const NODE_ENV = process.env.NODE_ENV || 'development';
const isProduction = NODE_ENV === 'production';

module.exports = withSass({
  publicRuntimeConfig: {
    NODE_ENV: NODE_ENV,
    API_URL: isProduction ? '/api/' : 'http://localhost:3000/',
    WS_URL: isProduction ? '/' : 'http://localhost:3000/'
  },
});