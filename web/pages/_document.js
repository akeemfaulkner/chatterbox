import Document, { Head, Main, NextScript } from 'next/document';
import React from 'react';

export default class MyDocument extends Document {

  render() {

    return (
        <html>
        <Head>
          <link href="https://fonts.googleapis.com/css?family=Thasadith" rel="stylesheet"/>
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        </Head>
        <body>
        <Main/>
        <NextScript/>
        </body>
        </html>
    );
  }

}
