import React from 'react';
import App, {Container} from 'next/app';
import withReduxStore from '../core/lib/with-redux-store';
import {Provider} from 'react-redux';
import '../styles/index.scss';
import withPageTransition from '../core/enhancers/withPageTransition';


class MyApp extends App {

  static async getInitialProps({Component, router, ctx}) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    return {pageProps};
  }


  render() {
    const {Component, pageProps, reduxStore} = this.props;
    return (
        <Container>
          <Provider store={reduxStore}>
            <Component {...pageProps} />
          </Provider>
        </Container>
    );
  }


}

export default withPageTransition(withReduxStore(MyApp));