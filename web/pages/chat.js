import React, {Component} from 'react';
import {UserType} from '../core/enhancers/withRouteGuard';
import withRouteGuard from '../core/enhancers/withRouteGuard';
import authentication from '../core/authentication';
import Toolbar from '../components/Toolbar';
import Card from '../components/Card';
import Dashboard from '../components/Dashboard';
import {getProfile} from '../core/state/profile/actionCreators';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {selectCurrentUser} from '../core/selectors';

class Chat extends Component {

  render() {
    const {profile} = this.props;
    return profile ? (
        <Card>
          <Toolbar logout={authentication.logout}/>
          <Dashboard/>
        </Card>
    ) : null;
  }

  componentDidMount() {
    this.props.getProfile();
  }

}

export default compose(
    connect(selectCurrentUser, {getProfile}),
    withRouteGuard(
        {
          redirectUrl: '/',
          userType: UserType.REGISTERED,
        }),
)(Chat);

