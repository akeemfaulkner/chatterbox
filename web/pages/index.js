import React, {Component} from 'react';
import IntroLoader from '../components/IntroLoader';
import Authentication from '../components/Authentication';
import http from '../core/http';
import PageTransition from '../components/PageTransition';
import withRouteGuard from '../core/enhancers/withRouteGuard';
import {UserType} from '../core/enhancers/withRouteGuard';

class Index extends Component {

  state = {
    loaded: false,
    displayTransition: false,
  };

  render() {

    return (
        <div>

          {/*<img src="/static/images/chatter_680530.png" alt="Chatterbox"/>*/}
          {/*<Authentication/>*/}
          <IntroLoader onComplete={this.onLoaderComplete}/>
          {this.state.loaded && <Authentication
          />}
        </div>
    );
  }

  onLoaderComplete = () => setTimeout(() => this.setState({loaded: true}), 100);

}

export default withRouteGuard(
    {
      redirectUrl: '/chat',
      userType: UserType.ANONYMOUS,
    })(Index);