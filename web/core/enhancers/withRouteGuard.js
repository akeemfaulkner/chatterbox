import React, {Component} from 'react';
import Router from 'next/router';
import authentication from '../authentication';

export const UserType = {
  REGISTERED: 'REGISTERED',
  ANONYMOUS: 'ANONYMOUS',
};

export default function withRouteGuard({redirectUrl, userType}) {

  return Child => {

    return class Wrapper extends Component {

      constructor(props) {
        super(props);

        if (process.browser) {
          if (!Wrapper.canPass()) {
            Router.push(redirectUrl);
          }
        }

      }

      static canPass(ctx) {
        let authorisation;
        if (process.browser) {
          authorisation = authentication.getAuthorisation();
        }

        if (ctx && ctx.req) {
          authorisation = authentication.getAuthorisationFromCookies(
              ctx.req.headers.cookie);
        }

        return userType === UserType.ANONYMOUS && !authorisation ||
            userType === UserType.REGISTERED && authorisation;
      }

      static async getInitialProps(ctx) {
        let pageProps = {};

        if (Child.getInitialProps) {
          pageProps = await Child.getInitialProps(ctx);
        }
        return {
          ...pageProps,
          withRouteGuard: {
            __error: !Wrapper.canPass(ctx),
          },
        };
      }

      render() {
        const {withRouteGuard, ...rest} = this.props;
        return !withRouteGuard.__error ? (
            <Child {...rest}/>
        ) : null;
      }
    };
  }
}