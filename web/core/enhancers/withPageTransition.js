import React, {Component} from 'react';
import Router from 'next/router';
import PageTransition from '../../components/PageTransition';

export default function withPageTransition(Child) {
  return class extends Component {
    static async getInitialProps(ctx) {
      let pageProps = {};

      if (Child.getInitialProps) {
        pageProps = await Child.getInitialProps(ctx);
      }
      return pageProps;
    }

    state = {
      displayTransition: false,
    };

    render() {
      return (
          <>
            <Child {...this.props}/>
            {this.state.displayTransition && <PageTransition/>}
          </>
      );
    }

    componentDidMount() {

      Router.events.on('routeChangeStart', () => {

        this.setState({displayTransition: true});

        Router.events.on('routeChangeComplete', () => {

          setTimeout(() =>
              this.setState({displayTransition: false}), 2000);

        });
      });

    }
  };
}