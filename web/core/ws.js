import io from 'socket.io-client';
import getConfig from 'next/config';

const { publicRuntimeConfig: {WS_URL} } = getConfig();
const ws = io(WS_URL);
export default ws;

