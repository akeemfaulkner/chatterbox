import axios from 'axios';
import authentication from './authentication';
import getConfig from 'next/config';
const { publicRuntimeConfig: {API_URL} } = getConfig();

const setAuthHeaders = (token, headers) => {
  if (token) {
    headers.common['Authorization'] = `Bearer ${token}`;
  }
};

const removeAuthHeaders = (headers) => {
  delete headers.common['Authorization'];
};

const http = axios.create({
  baseURL: API_URL,
  timeout: 1000,
  headers: {'Content-Type': 'application/json'},
  transformRequest: [
    function(data, headers) {

      if (process.browser) {
        const token = authentication.getAuthorisation();
        setAuthHeaders(token, headers);
      }

      if (headers['Content-Type'] === 'multipart/form-data') {
        return data;
      }

      return JSON.stringify(data);
    }],
});

http.setAuthorisation =
    token => {

      setAuthHeaders(token, http.defaults.headers);

    };

http.removeAuthorisation =
    () => {
      removeAuthHeaders(http.defaults.headers);

    };

export default http;