import {
  JOIN_CONVERSATION,
  NEW_MESSAGE,
  SEND_MESSAGE,
  SENDING_MESSAGE,
  UPDATING_MESSAGE,
} from '../state/conversation/actions';
import ws from '../ws';
import {getConversations} from '../state/conversation/actionCreators';

const poll = (func, time) => {
  return setInterval(func, time);
};
const chat = store => {

  ws.on('new_message', (message) => {
    store.dispatch({
      type: NEW_MESSAGE,
      payload: message,
    });
  });

  ws.on('join_conversation', () => {
    store.dispatch(getConversations());
  });

  poll(() => {
    const {profile} = store.getState();
    if(profile) {
      store.dispatch(getConversations());
    }
  }, 5000);

  let sendingMessageTimeout;
  ws.on('updating_message', () => {
    store.dispatch({
      type: UPDATING_MESSAGE,
      payload: true,
    });

    clearTimeout(sendingMessageTimeout);
    sendingMessageTimeout = setTimeout(() => {
      store.dispatch({
        type: UPDATING_MESSAGE,
        payload: false,
      });
    }, 500);
  });

  return next => action => {

    let result = next(action);

    if (action.type === `${JOIN_CONVERSATION}__succeeded`) {
      ws.emit('join_conversation', {conversationId: action.payload.id});
    }

    if (action.type === SENDING_MESSAGE) {
      ws.emit('sending_message', {conversationId: action.payload});
    }

    if (action.type === SEND_MESSAGE) {
      ws.emit('send_message', action.payload);
    }

    return result;
  };
};

export default chat;