export const getOtherUsers = ({users, profile}) => {

  return {
    users: {
      ...users,
      data: users.data.filter(user => user.id !== profile.id),
    },
  };
};

export const selectConversation = ({conversation}) => {
  return {
    conversation,
  };
};

export const selectCurrentUser = ({profile}) => {
  return {
    isCurrentUser: (user) => profile && user.id === profile.id,
    profile
  };
};

