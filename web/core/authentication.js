import Cookies from 'js-cookie';
import http from './http';
import Router from 'next/router';
import {AUTH_TOKEN_COOKIE_NAME} from '../../common/constants';
import {parseCookieFromString} from '../../common/utils';

const authentication = {
  setAuthorisation(token) {
    Cookies.set(AUTH_TOKEN_COOKIE_NAME, token, {expires: 30, path: ''});
  },
  getAuthorisation() {
    return Cookies.get(AUTH_TOKEN_COOKIE_NAME);
  },
  getAuthorisationFromCookies(cookies) {

    if (process.browser) {
      return Cookies.get(AUTH_TOKEN_COOKIE_NAME);
    }

    return parseCookieFromString(cookies, AUTH_TOKEN_COOKIE_NAME);
  },
  logout,
  login,
  signup,
};

function logout() {
  Cookies.remove(AUTH_TOKEN_COOKIE_NAME);
  http.removeAuthorisation();
  Router.push('/');
}

function login(data) {

  return http.
      post('/authorise', {
        'username': data.username,
        'password': data.password,
      }).
      then(({data}) => {
        http.setAuthorisation(data.token);
        if (process.browser) {
          authentication.setAuthorisation(data.token);
        }
        return Router.push('/chat');
      });
}

function signup(data) {
  return http.
      post('/users', {
        'username': data.username,
        'first_name': data.first_name,
        'last_name': data.last_name,
        'email': data.email,
        'password': data.password,
      }).
      then(() => {
        return login(data);
      });
}

export default authentication;