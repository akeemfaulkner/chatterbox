import {createStore, applyMiddleware, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import {reducer as formReducer} from 'redux-form';
import {profile} from './state/profile/reducer';
import {users} from './state/users/reducer';
import {conversation} from './state/conversation/reducer';
import chat from './middleware/chat';

export function initializeStore(initialState = {}) {
  return createStore(
      combineReducers({
        form: formReducer,
        profile,
        users,
        conversation
      }),
      initialState,
      composeWithDevTools(applyMiddleware(...[chat, thunkMiddleware])));
}