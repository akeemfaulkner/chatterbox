export const asyncAction = (actionType, fn) => (...args) => dispatch => {
  dispatch({
    type: `${actionType}__pending`,
  });

  return fn(...args).then(
      (res) => {
        dispatch({
          type: `${actionType}__succeeded`,
          payload: res.data,
        });
        return res.data;
      });
};

export function getLocale() {
  if (navigator.languages !== undefined)
    return navigator.languages[0];
  else
    return navigator.language;
}

export function unixTimestampToDate(timestamp) {
  return new Date(timestamp).toLocaleDateString(getLocale(), {
    day: 'numeric',
    month: 'short',
    year: 'numeric',
  });
}

export function hashUrlToObject() {
  const hash = window.location.hash.replace('#/', '');
  const values = hash.split('&');

  return values.reduce((prev, curr) => {
    const tuple = curr.split('=');
    const key = tuple[0];
    const value = tuple[1];

    return {...prev, [key]: value};
  }, {});
}