import React from 'react';
import {
  Field,
  getFormSubmitErrors,
  reduxForm,
  SubmissionError,
} from 'redux-form';
import InputGroup from '../../components/InputGroup';
import authentication from '../authentication';
import {connect} from 'react-redux';
import _ from 'lodash';
import getValidators from '../../../common/validators';
import http from '../http';
import {getProfile} from '../state/profile/actionCreators';

const validators = getValidators(_);

const renderField = (field) => (
    <InputGroup
        maxLength={field.maxLength}
        {...field.input}
        label={field.label}
        type={field.type}
        error={field.meta.touched && field.meta.error}
    />
);

export const InputField = (props) => (
    <Field
        {...props}
        component={renderField}
    />
);

function parseDuplicateUsernameError({response: {data: {data}}}) {
  let username;
  const alternatives = data.data.alternatives;

  if (alternatives) {

    const usernames = alternatives.
        slice(0, 2).
        join(' or ');

    username = `Username is in use. Free alternatives: ${usernames}.`;
  }

  return {
    _error: data.error,
    username,
  };
}


export function withLoginForm(Component) {
  const formName = 'login';

  const Form = reduxForm({
    form: formName,

    onSubmit: (data) => {
      return authentication.login(data).
          catch(res => {
            throw new SubmissionError({
              _error: _.get(res, 'response.data.data.error'),
            });
          });
    },
  })(Component);

  return connect(
      state => ({error: getFormSubmitErrors(formName)(state)}),
  )(Form);

}

export function withSignUpForm(Component) {

  const formName = 'signup';

  const Form = reduxForm({
    form: formName,
    validate: (values) => {

      const username = validators.validateForUsernameErrors(values.username);
      const password = validators.validateForPasswordErrors(values.password);
      const email = validators.validateForEmailErrors(values.email);

      const confirm_password = values.password !== values.confirm_password
          ? "Passwords do not match"
          : null;

      return {
        username,
        password,
        email,
        confirm_password
      };
    },
    onSubmit: (data) => {

      return authentication.signup(data).
          catch(res => {
            throw new SubmissionError(parseDuplicateUsernameError(res));
          });
    },
  })(Component);

  return connect(
      state => ({error: getFormSubmitErrors(formName)(state)}),
  )(Form);

}

export function withSettingsForm(Component) {

  const formName = 'settings';

  const Form = reduxForm({
    form: formName,
    validate: (values) => {

      const username = validators.validateForUsernameErrors(values.username);

      return {
        username,
      };

    },
    onSubmit: ({profile_photo, bio, username}, dispatch, {onSave}) => {

      const formData = new FormData();

      if (profile_photo) {
        formData.append('profile_photo', profile_photo, profile_photo.name);
      }

      formData.append('bio', bio);

      return http.put('/profile/username', {new_username: username}).
          then(() => {
            return http.put('/profile', formData,
                {headers: {'Content-Type': 'multipart/form-data'}}).
                then(() => {
                  onSave();
                  dispatch(getProfile());
                });
          }).
          catch((res) => {
            throw new SubmissionError(parseDuplicateUsernameError(res));
          });
    },
  })(Component);

  return connect(
      state => ({error: getFormSubmitErrors(formName)(state)}),
  )(Form);

}