import http from '../../http';
import {FIND_USERS, CLEAR_USERS} from './actions';
import {asyncAction} from '../../utils';

export const findUsers = asyncAction(FIND_USERS, (query) => http.get(`/users?q=${query}`));
export const clearUsers = ()=>({type: CLEAR_USERS});

