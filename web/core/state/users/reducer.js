import {CLEAR_USERS, FIND_USERS} from './actions';

export const users = (state = {data: []}, action) => {
  switch (action.type) {
    case `${FIND_USERS}__succeeded`:
      return {
          ...state,
        results: action.payload,
      };
    case CLEAR_USERS:
      return {
          ...state,
        results: null,
      };
    default:
      return state;
  }
};