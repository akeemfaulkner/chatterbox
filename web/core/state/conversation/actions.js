export const START_CONVERSATION = '[conversation] START_CONVERSATION';
export const GET_CONVERSATIONS = '[conversation] GET_CONVERSATIONS';
export const JOIN_CONVERSATION = '[conversation] JOIN_CONVERSATION';
export const SEND_MESSAGE = '[conversation] SEND_MESSAGE';
export const NEW_MESSAGE = '[conversation] NEW_MESSAGE';
export const SENDING_MESSAGE = '[conversation] SENDING_MESSAGE';
export const UPDATING_MESSAGE = '[conversation] UPDATING_MESSAGE';