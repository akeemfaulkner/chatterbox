import {
  GET_CONVERSATIONS,
  JOIN_CONVERSATION,
  NEW_MESSAGE,
  UPDATING_MESSAGE,
} from './actions';

export const conversation = (state = defaultState(), action) => {
  switch (action.type) {
    case `${GET_CONVERSATIONS}__succeeded`:
      return {
        ...state,
        list: action.payload,
      };
    case `${JOIN_CONVERSATION}__succeeded`:
      return {
        ...state,
        history: action.payload,
      };
    case NEW_MESSAGE:
      return {
        ...state,
        history: {
            ...state.history,
          messages: [...state.history.messages, action.payload],
          updating: false
        }
      };
    case UPDATING_MESSAGE:
      return {
        ...state,
        history: {
            ...state.history,
          updating: action.payload
        }
      };
    default:
      return state;
  }
};

function defaultState() {
  return {
    list: null,
    history: null
  };
}