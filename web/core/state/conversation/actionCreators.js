import http from '../../http';
import {
  GET_CONVERSATIONS,
  START_CONVERSATION,
  JOIN_CONVERSATION,
  SEND_MESSAGE, SENDING_MESSAGE,
} from './actions';
import {asyncAction} from '../../utils';
import ws from '../../ws';

export const joinConversation = asyncAction(JOIN_CONVERSATION,
    (conversationId) => http.get(`/conversation/${conversationId}`));

export const sendMessage = (conversationId, message) => ({
  type: SEND_MESSAGE,
  payload: {conversationId, message},
});

export const getConversations = asyncAction(GET_CONVERSATIONS,
    () => http.get('/conversation'));

export const updatingMessage = (conversationId) => ({
  type: SENDING_MESSAGE,
  payload: conversationId,
});

export const startConversation = (recipientUserId) => {

  return async dispatch => {
    const {data: {conversation}} = await asyncAction(START_CONVERSATION,
        (recipientUserId) => http.post('/conversation', {recipientUserId}))(
        recipientUserId)(dispatch);
    await getConversations()(dispatch);

    ws.emit('start_conversation',
        {conversationId: conversation, recipientId: recipientUserId});

    window.location.hash = `/conversation=${conversation}`;
  };
};
