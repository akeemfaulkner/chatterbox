import http from '../../http';
import {GET_PROFILE, UPDATE_PROFILE} from './actions';
import {asyncAction} from '../../utils';
import Router from 'next/router';
import authentication from '../../authentication';


export const updateProfile = asyncAction(UPDATE_PROFILE, () => http.put('/profile'));

export const getProfile = () => {

  return async dispatch => {

    try {
      await asyncAction(GET_PROFILE, () => http.get('/profile'))()(dispatch);
    } catch (e) {
      authentication.logout();
      Router.push('/')
    }

  };
};

