import {GET_PROFILE} from './actions';

export const profile = (state = null, action) => {
  switch (action.type) {
    case `${GET_PROFILE}__succeeded`:
      return action.payload;
    default:
      return state;
  }
};